""" Obj3D library

    This library can be used to create 3D objects with vertices, edges and faces and save them in .obj format.

"""

from copy import deepcopy
import numpy as np
from math import cos, sin, pi
import os

class Matrix(object):
    """ Matrix class
        
        This class provides functions for 3D transformation such as translation, rotation, scale
        
        Example :
        >>> m = Matrix()
        
    """
    def __init__(self):
        """ Matrix initialisation
            sets matrix to identity

            Example : 
            >>> m = Matrix()
            
        """
        self.reset()

    def translate(self, v):
        """ add translation to Matrix
            :param list v: 3 number list as translation vector

            Example :
            >>> m = Matrix()
            >>> m.translate((2,3,-1))
            >>> m.transform((5,0,2))
            (7.0, 3.0, 1.0)
        """        
        try:
            m = np.array(((1, 0, 0, 0), (0, 1, 0, 0), (0, 0, 1, 0),(v[0], v[1], v[2], 1)))
            self.matrix = np.dot(self.matrix, m)
        except:
            raise ValueError('Unexpected value ' + str(v))
   
    def scale(self, v, center=(0, 0, 0)):
        """ add scale to Matrix
            :param number v: scale factor
            :param list v: x,y and z scale
            :param list center: pivot center vertice

            Example :
            >>> m = Matrix()
            >>> m.scale(2)
            >>> m.transform((5,0,2))
            (10.0, 0.0, 4.0)
            >>> m.reset()
            >>> m.scale((2,1,3))
            >>> m.transform((5,0,2))
            (10.0, 0.0, 6.0)
            >>> m.reset()
            >>> m.scale(3,(1,0,0))
            >>> m.transform((5,0,2))
            (13.0, 0.0, 6.0)
        """        
        if isinstance(v, (int, float)):
            m = np.array(((v, 0, 0, 0), (0, v, 0, 0), (0, 0, v, 0),
                          (center[0] * (1 - v), center[1] * (1 - v),
                           center[2] * (1 - v), 1)))
        else:
            try:
                m = np.array(
                    ((v[0], 0, 0, 0), (0, v[1], 0, 0), (0, 0, v[2], 0),
                     (center[0] * (1 - v[0]), center[1] * (1 - v[1]),
                      center[2] * (1 - v[2]), 1)))
            except:
                raise ValueError('Unexpected value ' + str(v))
        self.matrix = np.dot(self.matrix, m)

    def rotateX(self, a, center=(0, 0, 0)):
        """ add rotation around x-axis to Matrix
            :param number a: rotation angle in radians
            :param list center: pivot center vertice

            Example :
            >>> m = Matrix()
            >>> m.rotateX(pi/2)
            >>> m.transform((5,-3,2))
            (5.0, -2.0, -3.0)
        """        

        m = np.array(
            ((1, 0, 0, 0), (0, cos(a), sin(a), 0), (0, -sin(a), cos(a), 0),
             (0, center[1] * (1 - cos(a)) + center[2] * sin(a),
              -center[1] * sin(a) + center[2] * (1 - cos(a)), 1)))
        self.matrix = np.dot(self.matrix, m)

    def rotateY(self, a, center=(0, 0, 0)):
        """ add rotation around y-axis to Matrix
            :param number a: rotation angle in radians
            :param list center: pivot center vertice

            Example :
            >>> m = Matrix()
            >>> m.rotateY(pi/2)
            >>> m.transform((5,-3,6))
            (6.0, -3.0, -5.0)
        """        

        m = np.array(
            ((cos(a), 0, -sin(a), 0), (0, 1, 0, 0), (sin(a), 0, cos(a), 0),
             (center[0] * (1 - cos(a)) - center[2] * sin(a), 0,
              center[0] * sin(a) + center[2] * (1 - cos(a)), 1)))
        self.matrix = np.dot(self.matrix, m)

    def rotateZ(self, a, center=(0, 0, 0)):
        """ add rotation around x-axis to Matrix
            :param number a: rotation angle in radians
            :param list center: pivot center vertice

            Example :
            >>> m = Matrix()
            >>> m.rotateZ(pi/2)
            >>> m.transform((5,-4,6))
            (4.0, 5.0, 6.0)
        """        

        m = np.array(
            ((cos(a), sin(a), 0, 0), (-sin(a), cos(a), 0, 0), (0, 0, 1, 0),
             (center[0] * (1 - cos(a)) + center[1] * sin(a),
              -center[0] * sin(a) + center[1] * (1 - cos(a)), 0, 1)))
        self.matrix = np.dot(self.matrix, m)

    def multiply(self,m):
        """ multiply self matrix times m
            used to compose various transformations as translation, rotation, scale
            :param Matrix m: matrix to right multiply

            Example :
            >>> m = Matrix()
            >>> m.translate((2,3,-1))
            >>> n = Matrix()
            >>> n.scale(2)
            >>> m.multiply(n)
            >>> m.transform((0,0,0))
            (4.0, 6.0, -2.0)
        """
        self.matrix = np.dot(self.matrix,m.matrix)

    def reset(self):
        """ sets the matrix to identity

            example : 
            >>> m = Matrix()
            >>> m.reset() 
            
        """
        self.matrix = np.eye(4)

    def transform(self, v):
        """ return vertice 3D transformation
            :param list v: x,y,z vertice coordinates

            Example :
            >>> m = Matrix()
            >>> m.rotateZ(pi/2,(1,0,0))
            >>> m.transform((5,-4,6))
            (5.0, 4.0, 6.0)
        """        
        
        return tuple(np.dot(np.array(v + (1, )), self.matrix))[:3]

    def copy(self):
        """ return a copy of Matrix object

            Example :
            >>> m = Matrix()
            >>> m.translate((2,7,-5.5))
            >>> n = m.copy()
        """

        m = Matrix()
        m.matrix = self.matrix.copy()
        return m

    def inverse(self):
        """ return a copy of inverse Matrix object

            Example :
            >>> m = Matrix()
            >>> m.translate((2,7,-5.5))
            >>> n = m.inverse()
        """

        m = Matrix()
        m.matrix = np.linalg.inv(self.matrix)
        return m

class Material(object):
    """ Material class
        
        This class manages material properties such as diffuse,ambient and specular colors
        
        Example :
        >>> red = Material(diffuse=(1.0,0.0,0.0))
        
    """

    default = {'diffuse':{'var':'Kd','value':(1.0,1.0,1.0)}}

    def __init__(self,**args):
        """ create a new material
            :param args: material parameters writen in key = value format
            available parameters :
            diffuse : object diffuse color in a rgb tuple, each channel level is between 0 and 1 
        
        Example :
        >>> blue = Material(diffuse=(0.0,0.0,1.0))
        """

        self.parameters = self.default.copy()
        self.set_param(**args)

    def set_param(self,**args):
        """ set a color parameter
            :param args: material parameters writen in key = value format
            available parameters :
            diffuse : object diffuse color in a rgb tuple, each channel level is between 0 and 1 

        Example :
        >>> orange = Material(diffuse=(1.0,0.55,0.0))
        """

        for key,value in args.items():
            if key in self.default.keys():
                self.parameters[key]['value'] = value
            else:
                raise ValueError('parameter ' + key + " is not valid")

    def get_value(self,name):
        """ get a color parameter value
            :param string name: parameter name

        Example :
        >>> orange = Material(diffuse=(1.0,0.55,0.0))
        >>> orange.get_value('diffuse')
        (1.0, 0.55, 0.0)
        """

        if name in self.default.keys():
            return self.parameters[name]['value']
        else:
            raise ValueError('parameter ' + name + " is not valid")

    def get_var(self,name):
        """ get a color parameter variable in mtl file
            :param string name: parameter name

        Example :
        >>> orange = Material(diffuse=(1.0,0.55,0.0))
        >>> orange.get_var('diffuse')
        'Kd'
        """

        if name in self.default.keys():
            return self.parameters[name]['var']
        else:
            raise ValueError('parameter ' + name + " is not valid")

class Obj3D(object):
    """ Obj3D class

        This class provides tools to create 3D meshes (vertices, edges and faces) and save them as .obj files
        
        Example:
        >>> obj3D = Obj3D()
        >>> obj3D.add_object('triangle')
        'triangle'
        >>> obj3D.add_face([(0,0,0),(10,0,0),(0,10,0)])
        >>> obj3D.save_as('triangle.obj')
    """

    def __init__(self, merge_distance=0.0001):
        """ initialisation of Obj3D
            :param float merge_distance: minimal distance between two vertices, lower this distance vertices will be merged, default value is 0.0001

            Example:
            >>> obj3D = Obj3D()
        """
        self.objects = {}
        self.meshes = {}
        self.materials = {}
        self.current_object = ''
        self.merge_distance = merge_distance

    def __new_name(self, name, where):
        """ return name if it doesn't exist in where dictionary
            else return name followed by . and a unique number

            private function 
        """
        if name in self.__dict__[where].keys():
            # si le nom existe deja
            if len(name.split('.')) == 1:
                new_name = [name, '1']
            else:
                try:
                    i = int(name.split('.')[-1])
                    new_name = ['.'.join(name.split('.')[0:-1]), str(i)]
                except ValueError:
                    new_name = [name, '1']
            while '.'.join(new_name) in self.__dict__[where].keys():
                new_name[1] = str(int(new_name[1]) + 1)
            return '.'.join(new_name)
        else:
            return name

    def add_object(self, name):
        """ add a 3D object and sets it as current object
            :param str name: object's name, if name already exists, change name by adding a number with '.x' format
            :returns: object name modified if necessary
            :rtype: str
            
            Example:
            >>> obj3D = Obj3D()
            >>> obj3D.add_object('cube')
            'cube'
            >>> obj3D.add_object('cube')
            'cube.1'
        """

        obj_name = self.__new_name(name, 'objects')
        mesh_name = self.__new_name(name, 'meshes')

        # création du mesh
        self.meshes[mesh_name] = {'vertices': [], 'lines': [], 'faces': []}
        # création de l'objet
        self.objects[obj_name] = {'matrix': Matrix(), 'mesh': mesh_name,'material':''}
        self.current_object = obj_name

        return obj_name

    def copy_object(self, source, dest=''):
        """ copy source 3D object to dest and sets it as current object
            copies are independant
            :param str source: object's name, must be an existing object else return an error
            :param str dest: copy's name, if string is empty create a new name, default value is empty
            
            Example:
            >>> obj3D = Obj3D()
            >>> obj3D.add_object('cube')
            'cube'
            >>> obj3D.copy_object('cube')
            'cube.1'
        """

        if source not in self.objects:
            raise ValueError('object ' + source + " doesn't exist")

        if not dest:
            dest = self.__new_name(source, 'objects')
        else:
            dest = self.__new_name(dest, 'objects')

        mesh_name = self.__new_name(dest, 'meshes')

        self.meshes[mesh_name] = deepcopy(
            self.meshes[self.objects[source]['mesh']])
        self.objects[dest] = {
            'matrix': self.objects[source]['matrix'].copy(),
            'mesh': mesh_name,'material':self.objects[source]['material']
        }
        self.current_object = dest
        return dest

    def clone_object(self, source, dest=''):
        """ clone source 3D object to dest and sets it as current object
            copies shares same mesh
            :param str source: object's name, must be an existing object else return an error
            :param str dest: copy's name, if string is empty create a new name, default value is empty
            
            Example:
            >>> obj3D = Obj3D()
            >>> obj3D.add_object('cube')
            'cube'
            >>> obj3D.clone_object('cube')
            'cube.1'
        """

        if source not in self.objects:
            raise ValueError('object ' + source + " doesn't exist")

        if not dest:
            dest = self.__new_name(source, 'objects')
        else:
            dest = self.__new_name(dest, 'objects')

        self.objects[dest] = {
            'matrix': self.objects[source]['matrix'].copy(),
            'mesh': self.objects[source]['mesh'],'material':self.objects[source]['material']
        }
        self.current_object = dest
        return dest

    def remove_object(self, name):
        """ remove an object from the list
            :param str name: object's name

            example:
            >>> obj3D = Obj3D()
            >>> obj3D.add_object('cube')
            'cube'
            >>> obj3D.remove_object('cube')        
        """
        # effacer mesh si plus utilisé
        mesh_name = self.objects[name]['mesh']
        n = 0
        for obj in self.objects.values():
            if obj['mesh'] == mesh_name:
                n += 1
        if n == 1:
            self.meshes.pop(mesh_name, 0)
        # efface l'objet
        self.objects.pop(name, 0)

        if self.current_object == name:
            self.current_object = ''

    def translate(self, v, name=''):
        """ add translation to name object 
            :param list v: 3 number list as translation vector
            :param string name: name object or current object when name is empty

            Example :
            >>> obj3D = Obj3D()
            >>> obj3D.add_object('cube')
            'cube'
            >>> obj3D.translate((2,3,-1))
        """        

        if not name:
            name = self.current_object

        if not name:
            raise ValueError('no object to translate')

        self.objects[name]['matrix'].translate(v)

    def scale(self, v, center=(0, 0, 0), name=''):
        """ add scale to name object 
            :param number v: scale factor
            :param list v: x,y and z scale
            :param list center: pivot center vertice
            :param string name: name object or current object when name is empty

            Example :
            >>> obj3D = Obj3D()
            >>> obj3D.add_object('cube')
            'cube'
            >>> obj3D.scale(5)
        """        
        if not name:
            name = self.current_object

        if not name:
            raise ValueError('no object to scale')

        self.objects[name]['matrix'].scale(v, center)

    def rotateX(self, a, center=(0, 0, 0), name=''):
        """ add rotation around x-axis to name object
            :param number a: rotation angle in radians
            :param list center: pivot center vertice
            :param string name: name object or current object when name is empty

            Example :
            >>> obj3D = Obj3D()
            >>> obj3D.add_object('cube')
            'cube'
            >>> obj3D.rotateX(pi/2)
        """
        if not name:
            name = self.current_object

        if not name:
            raise ValueError('no object to rotate')

        self.objects[name]['matrix'].rotateX(a, center)

    def rotateY(self, a, center=(0, 0, 0), name=''):
        """ add rotation around y-axis to name object
            :param number a: rotation angle in radians
            :param list center: pivot center vertice
            :param string name: name object or current object when name is empty

            Example :
            >>> obj3D = Obj3D()
            >>> obj3D.add_object('cube')
            'cube'
            >>> obj3D.rotateY(pi/2)
        """
        if not name:
            name = self.current_object

        if not name:
            raise ValueError('no object to rotate')

        self.objects[name]['matrix'].rotateY(a, center)

    def rotateZ(self, a, center=(0, 0, 0), name=''):
        """ add rotation around z-axis to name object
            :param number a: rotation angle in radians
            :param list center: pivot center vertice
            :param string name: name object or current object when name is empty

            Example :
            >>> obj3D = Obj3D()
            >>> obj3D.add_object('cube')
            'cube'
            >>> obj3D.rotateZ(pi/2)
        """
        if not name:
            name = self.current_object

        if not name:
            raise ValueError('no object to rotate')

        self.objects[name]['matrix'].rotateZ(a, center)

    def reset_transform(self, name=''):
        """ reset name object transformation
            :param string name: name object or current object when name is empty

            Example :
            >>> obj3D = Obj3D()
            >>> obj3D.add_object('cube')
            'cube'
            >>> obj3D.rotateZ(pi/2)
            >>> obj3D.reset_transform()

        """

        if not name:
            name = self.current_object

        if not name:
            raise ValueError('no object to reset')

        self.objects[name]['matrix'].reset()

    def add_material(self,name,material):

        mtl_name = self.__new_name(name, 'materials')
        self.materials[mtl_name] = material
        return mtl_name

    def set_material(self,mtl_name,name = ''):

        if not name:
            name = self.current_object

        if not name:
            raise ValueError('no object to set material')

        if not mtl_name in self.materials.keys():
            raise ValueError('unknown material '+mtl_name)

        self.objects[name]['material'] = mtl_name

    def add_vertice(self, v, name=''):
        """ add a vertice to name object if a vertice closer than merge_distance doesn't exists else return closest vertice 
            :param str name: object's name, optional parameter, current_object by default
            :param tuple v: vertice coordinates (x,y,z)
            :returns: vertice id (start 1 not 0 as in .obj files)
            :rtype: int
            
            Example:
            >>> obj3D = Obj3D()
            >>> obj3D.add_object('solid')
            'solid'
            >>> obj3D.add_vertice((1,0,0))
            1
            >>> obj3D.add_vertice((0,2,0))
            2
            >>> obj3D.add_vertice((1,0,0))
            1
        """
        if not name:
            name = self.current_object

        if not name:
            name = self.add_object('object')

        mesh = self.meshes[self.objects[name]['mesh']]

        found = None
        found_dist = 0
        for n, v2 in enumerate(mesh['vertices']):
            d = (v2[0] - v[0])**2 + (v2[1] - v[1])**2 + (v2[2] - v[2])**2
            if d < self.merge_distance**2:
                if found:
                    if d < found_dist:
                        found = n
                        found_dist = d
                else:
                    found = n
                    found_dist = d

        if found != None:
            return found + 1
        else:
            # ajoute le point à la liste des vertices
            mesh['vertices'].append(tuple(v))
            # renvoie le numéro du point, dernier de la liste
            return len(mesh['vertices'])

    def add_line(self, vlist, name=''):
        """ add a polyline to name object
            :param list vlist: vertice list, each vertice can be an integer (vertice index) or a 3 coordinates list
            :param str name: object's name, optional parameter, current_object by default

            Example:
            >>> obj3D = Obj3D()
            >>> obj3D.add_object('triangle')
            'triangle'
            >>> obj3D.add_vertice((1,0,0))
            1
            >>> obj3D.add_line([1,(0,1,0),(0,0,0),1])
         """
        if not name:
            name = self.current_object

        if not name:
            name = self.add_object('object')

        mesh = self.meshes[self.objects[name]['mesh']]

        line = []
        for vert in vlist:
            if isinstance(vert, int):
                if vert < 1 or vert > len(mesh['vertices']):
                    raise ValueError('vertice ' + str(vert) + " doesn't exist")
                line.append(vert)
            elif isinstance(vert, tuple) or isinstance(vert, list):
                line.append(self.add_vertice(vert))
            else:
                raise TypeError('unexpected type ' + str(type(vert)))
        mesh['lines'].append(tuple(line))

    def add_face(self, vlist, name=''):
        """ add a face to name object
            :param list vlist: vertice list, each vertice can be an integer (vertice index) or a 3 coordinates list
            :param str name: object's name, optional parameter, current_object by default

            Example:
            >>> obj3D = Obj3D()
            >>> obj3D.add_object('triangle')
            'triangle'
            >>> obj3D.add_vertice((1,0,0))
            1
            >>> obj3D.add_face([1,(0,1,0),(0,0,0)])
         """
        if not name:
            name = self.current_object

        if not name:
            name = self.add_object('object')

        mesh = self.meshes[self.objects[name]['mesh']]

        face = []
        for vert in vlist:
            if isinstance(vert, int):
                if vert < 1 or vert > len(mesh['vertices']):
                    raise ValueError('vertice ' + str(vert) + " doesn't exist")
                face.append(vert)
            elif isinstance(vert, tuple) or isinstance(vert, list):
                face.append(self.add_vertice(vert))
            else:
                raise TypeError('unexpected type ' + str(type(vert)))
        mesh['faces'].append(tuple(face))

    def set_parameter(self, **args):
        """ set Obj3D variable to new value
            :param args: arg is in "key = value" format

            Example:
                >>> obj3D = Obj3D()
                >>> obj3D.set_parameter(merge_distance=0.1)
                >>> print(obj3D.merge_distance)
                0.1
        """
        for key, value in args.items():
            self.__dict__[key] = value

    def has_clone(self,name=''):
        """
        return True if name object share its mesh with another object else return False (also if name object doesn't exist)
        :param str name: object's name, optional parameter, current_object by default

        Example:
            >>> obj3D = Obj3D()
            >>> obj3D.add_object('plane')
            'plane'
            >>> obj3D.add_face([(0,0,0),(1,0,0),(1,1,0),(0,1,0)])
            >>> obj3D.clone_object('plane','plane2')
            'plane2'
            >>> obj3D.has_clone('plane')
            True
        """
        if not name:
            name = self.current_object

        if name:
            mesh_name = self.objects[name]['mesh']
            mesh_count = 0
            for key,val in self.objects.items():
                if val['mesh'] == mesh_name:
                    mesh_count += 1
            if mesh_count == 1:
                return False
            else:
                return True
        else:
            return False

    def apply_transform(self,name=''):
        """ 
        apply all geometric transformation to vertices and sets transform matrix to identity
        can't apply if object has clone
        :param str name: object's name, optional parameter, current_object by default

        Example :
            >>> obj3D = Obj3D()
            >>> obj3D.add_object('plane')
            'plane'
            >>> obj3D.add_face([(0,0,0),(1,0,0),(1,1,0),(0,1,0)])
            >>> obj3D.translate((2,1,-1))
            >>> obj3D.apply_transform()
        """
        if not name:
            name = self.current_object

        if not name:
            raise ValueError('no object to apply transform')

## Amélioration du code
## ajouter un parametre unlink = False
## si unlink decloner si il y a des clone

        if self.has_clone():
            raise ValueError("can't apply transform to clones.")
        else:
            matrix = self.objects[name]['matrix']
            mesh_name = self.objects[name]['mesh']
            for i,v in enumerate(self.meshes[mesh_name]['vertices']):
                self.meshes[mesh_name]['vertices'][i] = matrix.transform(v)
            self.reset_transform(name)

    def object_list(self):
        """
        return object list

        Example:
            >>> obj3D = Obj3D()
            >>> obj3D.add_object('plane')
            'plane'
            >>> obj3D.copy_object('plane')
            'plane.1'
            >>> obj3D.clone_object('plane.1')
            'plane.2'
            >>> print(obj3D.object_list())
            ('plane', 'plane.1', 'plane.2')
        """
        return tuple(self.objects.keys())

    def join(self,name):
        """
        Join current and name objects meshes to current object and remove name object
        Can't join clones

        Example:
            >>> obj3D = Obj3D()
            >>> obj3D.add_object('plane')
            'plane'
            >>> obj3D.add_face([(0,0,0),(1,0,0),(1,1,0),(0,1,0)])
            >>> obj3D.add_object('plane2')
            'plane2'
            >>> obj3D.add_face([(0,0,1),(1,0,1),(1,1,1),(0,1,1)])
            >>> obj3D.join('plane')
        """

        if not self.current_object:
            raise ValueError("no current object")

        if self.has_clone(name) or self.has_clone(self.current_object):
            raise ValueError("can't join clones.")

        inv_dest = self.objects[self.current_object]['matrix'].inverse()        
        self.objects[name]['matrix'].multiply(inv_dest)
        self.apply_transform(name)

        mesh_name = self.objects[name]['mesh']
        mesh = self.meshes[mesh_name]
        vert_idx = []
        for v in mesh['vertices']:
            j = self.add_vertice(v)
            vert_idx.append(j)

        for line in mesh['lines']:
            new_line = [vert_idx[u-1] for u in line]
            self.add_line(new_line)

        for face in mesh['faces']:
            new_face = [vert_idx[u-1] for u in face]
            self.add_face(new_face)

        self.remove_object(name)

    def save_as(self, filename):
        """
            save objects in obj format

        Example:
            >>> obj3D = Obj3D()
            >>> obj3D.add_object('plane')
            'plane'
            >>> obj3D.add_face([(0,0,0),(1,0,0),(1,1,0),(0,1,0)])
            >>> # uncomment next line to really save to plane.obj
            >>> # obj3D.save_as('plane.obj')
        """

        if self.materials:
            fs = os.path.splitext(filename)
            with open(fs[0]+'.mtl', mode='w') as f:
                for key,value in self.materials.items():
                    f.write('newmtl '+key+'\n')
                    for k,v in value.parameters.items():
                        f.write(v['var']+' '+' '.join([str(i) for i in v['value']])+'\n')
                    f.write('\n')
            
        with open(filename, mode='w') as f:
            f.write('#made by obj3D tools\n')
            offset = 0
            for key, value in self.objects.items():
                f.write('o ' + key + '\n')
                matrix = value['matrix']
                mesh_name = value['mesh']
                mesh = self.meshes[mesh_name]

                for v in mesh['vertices']:
                    f.write('v')
                    vt = matrix.transform(v)
                    for x in vt:
                        f.write(' ' + str(x))
                    f.write('\n')
                for v in mesh['lines']:
                    f.write('l')
                    for x in v:
                        f.write(' ' + str(offset + x))
                    f.write('\n')
                f.write('s off\n')
                if value['material']:
                    f.write('usemtl '+value['material']+'\n')
                for v in mesh['faces']:
                    f.write('f')
                    for x in v:
                        f.write(' ' + str(offset + x))
                    f.write('\n')
                offset += len(mesh['vertices'])


if __name__ == '__main__':
    import doctest
    doctest.testmod()
