""" Tree module

    This module provides tools to build trees using Obj3D library and export them in .obj format
    
"""

from random import random, uniform
from math import pi, cos, sin
import json

import obj3D

def between(p):
    """
    return a random value in p range
    :param list p: 2 number list as a range
    
    Example :
    >>> p = [0.5,0.7]
    >>> result = between(p)
    """
    return uniform(p[0], p[1])


class Branch(object):
    """ Branch class manages tree's Branch
        Branch is used by Tree class

        Example :
        >>> branch = Branch(None,10,5,pi/12,0)
    """

    def __init__(self, parent, length, diameter, xrot, yrot):
        """ Initialize a new Branch
            :param Branch parent: new branch will be linked to parent branch
            :param float length : branch length
            :param float diameter : branch mean diameter
            :param float xrot : branch rotation in radians along local x-axis 
            :param float yrot : branch rotation in radians along local y-axis 

            Example :
            >>> branch = Branch(None,10,5,pi/12,0)
        """
        self.parent = parent
        if parent:
            parent.children.append(self)
        self.length = length
        self.diameter = diameter
        self.xrot = xrot
        self.yrot = yrot
        self.children = []

    def get_matrix(self):
        """ return branch global transformation matrix including branch and parents transformations

            Example :
            >>> first = Branch(None,10,5,0,0)
            >>> second= Branch(first,7,3,0,0)
            >>> second.get_matrix().matrix[3][2]
            17.0
        """
        b = obj3D.Matrix()
        b.translate((0, 0, self.length))
        b.rotateX(self.xrot)
        b.rotateY(self.yrot)

        if self.parent:
            m = self.parent.get_matrix()
            b.multiply(m)
            return b
        else:
            return b

    def make_faces(self, sketch, previous, source):
        """ add branch mesh to sketch
            :param Obj3D sketch: sketch receive branch mesh
            :param list previous: vertices list of previous branch used for mesh connection
            :param string source: connection type, values are 'none','left','right' and 'center'

            :returns: end branch vertices  
            :rtype: list

            Example :
            >>> import obj3D
            >>> branch_mesh = obj3D.Obj3D()
            >>> branch_mesh.add_object('branch')
            'branch'
            >>> my_branch = Branch(None,8,4,pi/24,pi/18)
            >>> end = my_branch.make_faces(branch_mesh,[],'none')
            >>> len(branch_mesh.meshes['branch']['vertices'])
            16
        """
        m = obj3D.Matrix()
        m.rotateX(self.xrot)
        m.rotateY(self.yrot)

        if source == 'left':
            if self.xrot < 0:
                left_range = [9, 8, 1, 2, 3, 4, 5, 10]
            else:
                left_range = [9, 11, 3, 4, 5, 6, 7, 12]

        if source == 'right':
            if self.xrot < 0:
                right_range = [0, 1, 2, 3, 11, 9, 12, 7]
            else:
                right_range = [0, 1, 8, 9, 10, 5, 6, 7]

        if source == 'none':
            start = 0
        elif source == 'center':
            start = 0.1
        else:
            start = 0.3

        if previous:
            diameter = self.diameter * start + self.parent.diameter * (1 - start)
        else:
            diameter = self.diameter
            a = [m.transform((diameter * cos(2 * i * pi / 8),
                  diameter * sin(2 * i * pi / 8), 0)) for i in range(8)]
            sketch.add_face([a[0], a[1], a[2], a[7]])
            sketch.add_face([a[2], a[3], a[6], a[7]])
            sketch.add_face([a[3], a[4], a[5], a[6]])

        if previous:
            for i in range(8):
                if source == 'center':
                    a = [previous[i], previous[(i + 1) % 8]]
                elif source == 'left':
                    a = [
                        previous[left_range[i]],
                        previous[left_range[(i + 1) % 8]]
                    ]
                elif source == 'right':
                    a = [
                        previous[right_range[i]],
                        previous[right_range[(i + 1) % 8]]
                    ]
                a.append(
                    m.transform((diameter * cos(2 * (i + 1) * pi / 8),
                                 diameter * sin(2 * (i + 1) * pi / 8),
                                 self.length * start)))
                a.append(
                    m.transform(
                        (diameter * cos(2 * i * pi / 8),
                         diameter * sin(2 * i * pi / 8), self.length * start)))

                if self.parent:
                    p = self.parent.get_matrix()
                    for i in range(2, len(a)):
                        a[i] = p.transform(a[i])

                sketch.add_face(a)

        for i in range(8):
            a = [
                m.transform(
                    (diameter * cos(2 * i * pi / 8),
                     diameter * sin(2 * i * pi / 8), self.length * start))
            ]
            a.append(
                m.transform((diameter * cos(2 * (i + 1) * pi / 8),
                             diameter * sin(2 * (i + 1) * pi / 8),
                             self.length * start)))
            a.append(
                m.transform((self.diameter * cos(2 * (i + 1) * pi / 8),
                             self.diameter * sin(2 * (i + 1) * pi / 8),
                             self.length * 0.9)))
            a.append(
                m.transform(
                    (self.diameter * cos(2 * i * pi / 8),
                     self.diameter * sin(2 * i * pi / 8), self.length * 0.9)))

            if self.parent:
                p = self.parent.get_matrix()
                for i in range(len(a)):
                    a[i] = p.transform(a[i])

            sketch.add_face(a)

        finish = []
        for i in range(8):
            finish.append(
                m.transform(
                    (self.diameter * cos(2 * i * pi / 8),
                     self.diameter * sin(2 * i * pi / 8), self.length * 0.9)))
        for i in range(3, 6):
            t = obj3D.Matrix()
            t.rotateZ(-pi / 4)
            finish.append(
                m.transform(
                    t.transform((0, self.diameter * sin(2 * i * pi / 8),
                                 self.length * 0.9 -
                                 self.diameter * 2 * cos(2 * i * pi / 8)))))
        for i in [3, 5]:
            t = obj3D.Matrix()
            t.rotateZ(pi / 4)
            finish.append(
                m.transform(
                    t.transform((0, self.diameter * sin(2 * i * pi / 8),
                                 self.length * 0.9 -
                                 self.diameter * 2 * cos(2 * i * pi / 8)))))

        if self.parent:
            p = self.parent.get_matrix()
            for i in range(len(finish)):
                finish[i] = p.transform(finish[i])

        return finish

    def close(self, sketch, previous):
        """ close last branch mesh
            :param Obj3D sketch: sketch receive branch mesh
            :param list previous: vertices list of previous branch used for mesh connection

            Example :
            >>> import obj3D
            >>> branch_mesh = obj3D.Obj3D()
            >>> branch_mesh.add_object('branch')
            'branch'
            >>> my_branch = Branch(None,8,4,pi/24,pi/18)
            >>> end = my_branch.make_faces(branch_mesh,[],'none')
            >>> my_branch.close(branch_mesh,end)
        """
        sketch.add_face([previous[0], previous[1], previous[2], previous[7]])
        sketch.add_face([previous[2], previous[3], previous[6], previous[7]])
        sketch.add_face([previous[3], previous[4], previous[5], previous[6]])


    def draw(self, sketch, previous, source):
        """ add branch and all its children meshes to sketch
            :param Obj3D sketch: sketch receive branch mesh
            :param list previous: vertices list of previous branch used for mesh connection
            :param string source: connection type, values are 'none','left','right' and 'center'

            Example :
            >>> import obj3D
            >>> branch_mesh = obj3D.Obj3D()
            >>> branch_mesh.add_object('branch')
            'branch'
            >>> first = Branch(None,8,4,0,0)
            >>> second = Branch(first,5,3,0,0)
            >>> first.draw(branch_mesh,[],'none')
            >>> len(branch_mesh.meshes['branch']['vertices'])
            32
        """
        finish = self.make_faces(sketch, previous, source)

        # dessin des branches enfants
        if len(self.children) == 1:
            self.children[0].draw(sketch, finish, 'center')
        elif len(self.children) == 2:
            self.children[0].draw(sketch, finish, 'right')
            self.children[1].draw(sketch, finish, 'left')
        else:
            self.close(sketch, finish)

class Tree(object):
    """ Tree class provides tools to generate tree mesh and save it in .obj format

        Example :
        >>> my_tree = Tree(deep=3)
        >>> # uncomment next line to really export to my_tree.obj
        >>> # my_tree.export('my_tree.obj')
    """

    default = {
        "fork": 0.8,
        "xrot_cont": [-20 * pi / 180, 20 * pi / 180],
        "yrot_cont": [-20 * pi / 180, 20 * pi / 180],
        "xrot_fork": [10 * pi / 180, 50 * pi / 180],
        "yrot_fork": [10 * pi / 180, 50 * pi / 180],
        "child_length": [0.7, 0.9],
        "child_diameter": [0.6, 0.8],
        "init_length": 10,
        "init_diameter": 1.5,
        "init_xrot": [0, 0],
        "init_yrot": [0, 0],
        "deep": 4
    }

    def __init__(self, **args):
        """ create a new tree with parameters
            :param args: tree parameters writen in key = value format
            available parameters :
            fork : probability to create 2 branches instead of 1 at the end of a branch, default is 0.8
            xrot_cont : [min,max] interval in radians of x-axis random rotation of each new branch without fork, default is [-20*pi/180,20*pi/180]
            yrot_cont : [min,max] interval in radians of y-axis random rotation of each new branch without fork, default is [-20*pi/180,20*pi/180]
            xrot_fork : [min,max] interval in radians of x-axis random rotation of each new branch without fork, default is [10*pi/180,50*pi/180]
            yrot_fork : [min,max] interval in radians of y-axis random rotation of each new branch without fork, default is [10*pi/180,50*pi/180]
            
            Example :
            >>> my_tree = Tree(init_length=20,deep=3)
        """

        self.parameters = self.default.copy()

        if 'optionsfile' in args.keys():
            with open(args['optionsfile'],'r') as f:
                option_dict = json.loads(f.read())
                for key, value in option_dict.items():
                    self.parameters[key] = value

        for key, value in args.items():
            self.parameters[key] = value

        self.main_branch = Branch(None, self.parameters["init_length"],
                                  self.parameters["init_diameter"],
                                  between(self.parameters["init_xrot"]),
                                  between(self.parameters["init_yrot"]))

        self.__generate(self.main_branch, self.parameters["deep"])

    def __generate(self, branch, deep):
        """" calculate the tree
            :param Branch branch: current branch (recursive call)
            :param int deep: deep of recursivity

            private function
        """

        if random() < 0.5:
            s = -1
        else:
            s = 1

        e = random()
        if e > self.parameters["fork"]:
            xrot = s * between(self.parameters["xrot_cont"])
            yrot = between(self.parameters["yrot_cont"])
            Branch(branch,
                    branch.length * between(self.parameters["child_length"]),
                    branch.diameter *
                    between(self.parameters["child_diameter"]), xrot, yrot)
        else:
            xrot = s * between(self.parameters["xrot_fork"])
            yrot = between(self.parameters["yrot_fork"])
            Branch(branch,
                    branch.length * between(self.parameters["child_length"]),
                    branch.diameter *
                    between(self.parameters["child_diameter"]), xrot, yrot)
            Branch(branch,
                    branch.length * between(self.parameters["child_length"]),
                    branch.diameter *
                    between(self.parameters["child_diameter"]), -xrot, -yrot)

        if deep > 0:
            for child in branch.children:
                self.__generate(child, deep - 1)

    def export(self, filename):
        """ calculate and export tree mesh in .obj format
            :param string filename: export file name with .obj extension

            Example :
            >>> import tree
            >>> my_tree = tree.Tree(deep=3)
            >>> # uncomment next line to really export to my_tree.obj
            >>> # my_tree.export('my_tree.obj')
        """
        sketch = obj3D.Obj3D()
        sketch.add_object('tree')
        self.main_branch.draw(sketch, [], 'none')
        sketch.save_as(filename)

if __name__ == '__main__':
    import doctest
    doctest.testmod()
