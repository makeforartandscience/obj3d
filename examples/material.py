import obj3D

obj = obj3D.Obj3D()
red = obj3D.Material(diffuse=(1.0,0.0,0.0))
obj.add_material('red',red)

obj.add_object('triangle')
obj.set_material('red')
obj.add_face([(1,0,0),(1,1,0),(0,1,0)])

obj.save_as('triangle.obj')
