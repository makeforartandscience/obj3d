import obj3D
from math import pi

obj = obj3D.Obj3D()

obj.add_object('triangle')
obj.add_vertice((1,0,0))
obj.add_vertice((1,1,0))
obj.add_vertice((0,1,0))
obj.add_face([1,2,3])

obj.clone_object('triangle')
obj.translate((2,5,-3))

obj.copy_object('triangle')
obj.translate((2,5,-3))
obj.apply_transform()

obj.copy_object('triangle.1')
n = obj.objects[obj.current_object]['matrix'].inverse()
obj.add_vertice(n.transform((5,2,-1)))

obj.save_as('triangles.obj')
